# Usage

```text
Usage: mksshuser [args]
    -u NAME, --user=NAME             username to create (default 'ssh-admin')
    -S PATH, --sshd-config=PATH      path to sshd config (default '/etc/ssh/sshd_config')
    -k PATH, --key-file=PATH         path to public key file, or - for stdin (default '-')
    -?, --help                       Show this help
    -v, --version                    Display version
```
