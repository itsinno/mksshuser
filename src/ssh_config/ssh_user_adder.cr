require "./ssh_config_parser"

module MkSSHUser
  class SSHUserAdder
    getter parser

    def initialize(sshd_file : String)
      @file = sshd_file
      @parser = SSHConfigParser.new(@file)
    end

    def user?(name : String) : Bool
      blocks = @parser.find_blocks("AllowUsers")
      if blocks.size < 1
        false
      else
        found = false

        blocks.each do |block|
          if block.values.includes? name
            found = true
            break
          end
        end

        found
      end
    end

    def user_rules?(name : String) : Bool
      user_rules(name).size > 0
    end

    def user_rules(name : String) : Array(SSHBlock)
      blocks = @parser.find_blocks("Match")
        .select { |block| block.values[0] == "User" }
        .select { |block| block.values[1] == name }
        .map { |block| block.subblocks }
        .flatten
    end

    def <<(name : String) : Void
      return if user?(name)

      blocks = @parser.find_blocks("AllowUsers")
      values = [] of String

      blocks.each { |block| values += block.values }

      values << name

      @parser.remove_blocks("AllowUsers")
      @parser.add_block("AllowUsers", values)
    end

    def add_user_rules(name : String, rules : Array(SSHBlock)) : Void
      old_rules = user_rules(name)
      @parser.remove_blocks_by do |block|
        block.name == "Match" &&
          block.values[0] == "User" &&
          block.values[1] == name
      end

      @parser.add_block("Match", ["User", name])
        .subblocks = [old_rules, rules].flatten
    end

    def data
      @parser.data
    end
  end
end
