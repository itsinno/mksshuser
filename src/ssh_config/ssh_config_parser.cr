module MkSSHUser
  class SSHBlock
    INDENT = "  "

    getter name
    getter values
    getter subblocks
    setter subblocks

    def initialize(name : String, values : Array(String))
      @name = name
      @values = values
      @subblocks = [] of SSHBlock
    end

    def stringify(indent_level = 0) : String
      indent = INDENT * indent_level
      values = @values.join(' ')
      head = "%s%s %s" % [indent, @name, values]
      body = dump_body(indent_level + 1)

      [head, body].join("\n")
    end

    def dump_body(indent_level = 0)
      body = @subblocks
        .map { |value| value.stringify(indent_level) }
        .join
    end
  end

  class SSHConfigParser
    @data : SSHBlock

    getter data

    def initialize(file : String)
      @file = file
      @data = parse(File.read(file).strip)
    end

    def find_blocks(name : String) : Array(SSHBlock)
      @data.subblocks.select { |block| block.name == name }
    end

    def remove_blocks(name : String) : Void
      @data.subblocks = @data.subblocks.reject { |block| block.name == name }
    end

    def remove_blocks_by : Void
      @data.subblocks = @data.subblocks.reject { |block| yield block }
    end

    def add_block(name : String, values : Array(String)) : SSHBlock
      block = SSHBlock.new(name, values)

      @data.subblocks << block

      block
    end

    private def parse(data) : SSHBlock
      lines = data.split(/[\r\n]+/)
        .reject { |v| v.starts_with?(/[\s]*#/) }

      root_block = SSHBlock.new("#root", [] of String)
      root_block.subblocks =
        parse_block(lines, indent_of(lines.first))

      root_block
    end

    private def indent_of(line : String) : String
      line.gsub(/[\S][\s\S]+$/, "")
    end

    private def parse_block(lines : Array(String), indent) : Array(SSHBlock)
      blocks = [] of SSHBlock
      last_block = nil

      loop do
        break if lines.size < 1

        line = lines.first
        line_indent = indent_of line

        if line_indent == indent
          values = line.strip
            .sub('=', ' ')
            .split(/[\s]+/)
          name = values.shift

          last_block = SSHBlock.new(name, values)
          blocks << last_block

          lines.shift
        elsif line_indent.size < indent.size
          break
        else
          break if last_block.nil?
          last_block.subblocks = parse_block(lines, line_indent)
        end
      end

      blocks
    end
  end
end
