require "digest"

module MkSSHUser
  def self.read_file_or_stdin(path : String)
    path == "-" ? STDIN.gets_to_end : File.read path
  end

  def self.fail(message : String | Nil)
    STDERR.puts message
    exit -1
  end

  def self.check_file(path : String, digest : String)
    cont = File.read(path)
    digest(cont) == digest
  end

  def self.digest(data : String)
    [
      Digest::MD5.base64digest(data),
      Digest::SHA1.base64digest(data),
    ].join(':')
  end
end
