require "file_utils"

module MkSSHUser
  # class RuntimeError < Exception
  # end

  class AddGrantedUser
    alias Passwd = NamedTuple(
      exists: Bool,
      login: String | Nil,
      uid: Int32 | Nil,
      gid: Int32 | Nil,
      home: String | Nil,
      shell: String | Nil)

    EMPTY_PASSWD = Passwd.new(
      exists: false,
      login: nil,
      uid: nil,
      gid: nil,
      home: nil,
      shell: nil
    )

    private PATH_SEP    = ':'
    private SUDOERS_DIR = "/etc/sudoers.d"
    private SUDO_PERMS  = "ALL=(ALL:ALL) NOPASSWD:ALL"
    private BASHRC_PATH = "export PATH=$PATH:%s"

    def initialize(username : String)
      @username = username
    end

    def exists? : Bool
      passwd[:exists]
    end

    def create : Bool
      raise "cannot overwrite user" if exists?
      useradd = which("useradd")
      bash = which("bash")

      return false unless execute(useradd, "#{@username} -s #{bash}") == 0

      home = passwd[:home].as String

      FileUtils.rm_rf home if File.exists? home
      FileUtils.mkdir_p home

      return false unless File.directory? home

      File.chown home, passwd[:uid].as(Int32), passwd[:gid].as(Int32)

      true
    end

    def grant : Bool
      raise "cannot promote nonexistent user" unless exists?

      sudo = which("sudo")

      file = File.join(SUDOERS_DIR, @username)
      string = "#{@username} #{SUDO_PERMS}"

      File.write file, string, 0o0440

      File.file? file
    end

    def add_binpath : Bool
      raise "cannot add paths for nonexistent user" unless exists?

      bin_dir = File.join(passwd[:home].as(String), ".bin")

      FileUtils.mkdir_p bin_dir
      File.chown bin_dir, passwd[:uid].as(Int32), passwd[:gid].as(Int32)

      bashrc_file = File.join(passwd[:home].as(String), ".bash_profile")

      bashrc = File.file?(bashrc_file) ? File.read(bashrc_file).split(/[\r\n]+/) : ([] of String)
      bashrc << BASHRC_PATH % [bin_dir]

      File.write bashrc_file, bashrc.join('\n'), 0o0644
      File.chown bashrc_file, passwd[:uid].as(Int32), passwd[:gid].as(Int32)

      File.file? bashrc_file
    end

    def write_key(key : String)
      raise "cannot add key for nonexistent user" unless exists?

      ssh_dir = File.join passwd[:home].to_s, ".ssh"
      file = File.join ssh_dir, "authorized_keys"

      unless File.directory? ssh_dir
        FileUtils.rm ssh_dir if File.exists? ssh_dir
        FileUtils.mkdir_p ssh_dir
        File.chown ssh_dir, passwd[:uid].as(Int32), passwd[:gid].as(Int32)
      end

      existing_keys = [] of String
      existing_keys = File.read(file).split(/[\r\n]+/) if File.file? file
      existing_keys.reject! { |key| key.size < 1 }

      existing_keys << key

      File.write file, existing_keys.join('\n'), 0o0640
      File.chown file, passwd[:uid].as(Int32), passwd[:gid].as(Int32)

      File.file? file
    end

    private def passwd : Passwd
      getent = which("getent")
      ent = popen(getent, "passwd #{@username} 2>/dev/null").split ':'

      return EMPTY_PASSWD if ent.size < 2 && ent.first.size < 1

      Passwd.new(
        exists: true,
        login: ent[0],
        uid: ent[2].to_i,
        gid: ent[3].to_i,
        home: ent[5],
        shell: ent[6],
      )
    end

    private def which(name : String) : String
      path = ENV["PATH"].split(PATH_SEP)

      path.map { |p| File.join(p, name) }
        .select { |file| try_executable(file) }
        .first
    end

    private def try_executable(path : String)
      File.file? path
    end

    private def popen(exe : String, args : String = "")
      cmd = [exe, args].join ' '
      `#{cmd}`
    end

    private def execute(exe : String, args : String = "")
      cmd = [
        exe,
        args,
        ">/dev/null 2>/dev/null",
      ].join ' '
      system cmd
      $?.exit_status
    end
  end
end
