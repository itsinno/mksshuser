require "file_utils"
require "option_parser"

require "./ssh_config/ssh_user_adder"
require "./unix/add_granted_user"
require "./utils.cr"

module MkSSHUser
  VERSION    = "0.2.1"
  EXECUTABLE = "mksshuser"

  begin
    sshd_file = "/etc/ssh/sshd_config"
    user = "ssh-admin"
    key_file = "-"

    OptionParser.parse! do |parser|
      parser.banner = "Usage: #{EXECUTABLE} [args]"

      parser.on("-u NAME", "--user=NAME", "username to create (default '#{user}')") { |name| user = name }

      parser.on("-S PATH", "--sshd-config=PATH", "path to sshd config (default '#{sshd_file}')") { |path| sshd_file = path }
      parser.on("-k PATH", "--key-file=PATH", "path to public key file, or - for stdin (default '#{key_file}')") { |path| key_file = path }

      parser.on("-?", "--help", "Show this help") do
        puts parser
        exit 0
      end

      parser.on("-v", "--version", "Display version") do
        puts "#{EXECUTABLE}, version #{VERSION}"
        exit 0
      end
    end

    bak_file = "#{sshd_file}.mksshuser.bak"

    fail("must run as root") unless ENV["USER"] == "root"

    granter = AddGrantedUser.new user

    # creating user

    print "creating user '#{user}'... "

    fail("failed") unless granter.create

    puts "ok"

    # granting sudo access

    print "adding '#{user}' to sudoers... "

    fail("failed") unless granter.grant

    puts "ok"

    # adding ssh pubkey

    print "adding ssh key for '#{user}'... "

    key = read_file_or_stdin(key_file)
    fail("cannot read key") unless key.size > 0
    fail("failed") unless granter.write_key(key)

    puts "ok"

    # configuring local bindir

    print "  configuring local bin... "

    fail("failed") unless granter.add_binpath

    puts "ok"

    # adding user to sshd config

    puts "configuring ssh access for '#{user}'... "

    adder = SSHUserAdder.new(sshd_file)

    # no root login

    print "  denying root login via ssh... "

    adder.parser.remove_blocks "PermitRootLogin"
    adder.parser.add_block "PermitRootLogin", ["no"]

    puts "ok"

    # adding to AllowUsers

    print "  allowing ssh for #{user}... "

    adder << user

    puts "ok"

    # user configuration

    print "  adding user-specific options... "

    adder.add_user_rules(user, [
      SSHBlock.new("PasswordAuthentication", ["no"]),
      SSHBlock.new("PermitTTY", ["yes"]),
      SSHBlock.new("X11Forwarding", ["no"]),
    ])

    puts "ok"

    # create backup file

    print "  saving backup file... "

    FileUtils.rm_rf bak_file if File.exists? bak_file

    fail("failed") if File.exists? bak_file

    FileUtils.cp sshd_file, bak_file

    fail("failed") unless File.file? bak_file

    puts "ok"

    # applying changes

    print "  applying changes... "

    FileUtils.rm_rf sshd_file if File.exists? sshd_file

    fail("failed") if File.exists? sshd_file

    FileUtils.touch sshd_file

    fail("failed") unless File.file? sshd_file
    fail("failed") unless File.writable? sshd_file

    data = adder.data.dump_body
    digest = digest(data)

    File.write sshd_file, data

    unless check_file(sshd_file, digest)
      puts "failed"
      print "restoring backup file... "

      msg = "failed\n!! could not restore backup file; seems you have big problems"

      FileUtils.rm_rf sshd_file if File.exists? sshd_file

      fail(msg) if File.exists? sshd_file

      FileUtils.mv bak_file, sshd_file

      fail(msg) unless File.file? sshd_file

      fail("restored \n!! ssh config remains unchanged, you must modify it byself")
    end

    puts "ok"
  rescue e
    fail e.message
  end
end
