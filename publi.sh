#!/usr/bin/env bash

URL=https://api.bitbucket.org/2.0/repositories/itsinno/mksshuser/downloads
FILE=bin/mksshuser
FLAGS='-v --verbose'

if [[ "$1" == 'arch' ]]; then
  FLAGS="$FLAGS --threads=1"
fi

shards build $FLAGS
mv $FILE bin/mksshuser-$1

curl -v -u $2 -X POST $URL -F files=@bin/mksshuser-$1
